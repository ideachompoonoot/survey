package com.example.chompoonoot.surveys;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import com.example.chompoonoot.surveys.model.Question;
import com.example.chompoonoot.surveys.model.SurveyResponse;
import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.gson.Gson;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;


public class MainActivity extends AppCompatActivity {
    private List<SurveyResponse> surveyList;
    @BindView(R.id.listview)
    ListView listView;
    @BindView(R.id.refresh_button)
    ImageButton rrefreshButton;
    @BindView(R.id.survey_button)
    Button surveyButton;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        prepareForData();

        surveyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            Intent intent = new Intent(getApplicationContext(), QuestionActivity.class);
                startActivity(intent);

            }
        });

        rrefreshButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                prepareForData();
                Toast.makeText(getApplicationContext(), "Refreshing data ...", Toast.LENGTH_LONG).show();
            }
        });

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }
    protected void prepareForData(){
        ConnectionManager connectionManager = new ConnectionManager();
        connectionManager.prepareDataSource(new ConnectionManagerListener() {
            @Override
            public void onSuccess(Response<List<SurveyResponse>> response) {
                Log.d("test", response.toString());
                surveyList = response.body();
                //refresh screen after this point
                setUpView(surveyList);
                Toast.makeText(getApplicationContext(), "service has been called", Toast.LENGTH_LONG).show();
                System.out.println("OnSuccess called");
            }

            @Override
            public void onFailure(Throwable error) {
                Toast.makeText(getApplicationContext(), "error " + error.getMessage(), Toast.LENGTH_LONG).show();

                error.printStackTrace();
            }
        });
    }

    protected void setUpView(List<SurveyResponse> surveyList) {
        SurveyListViewAdapter adapter = new SurveyListViewAdapter(getLayoutInflater(), surveyList, this);
        this.listView.setAdapter(adapter);
        this.listView.setOnScrollChangeListener(new View.OnScrollChangeListener() {
            @Override
            public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                Log.d("ScrollViewChanged", scrollY+"");
            }
        });


    }

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    public Action getIndexApiAction() {
        Thing object = new Thing.Builder()
                .setName("Main Page") // TODO: Define a title for the content shown.
                // TODO: Make sure this auto-generated URL is correct.
                .setUrl(Uri.parse("http://[ENTER-YOUR-URL-HERE]"))
                .build();
        return new Action.Builder(Action.TYPE_VIEW)
                .setObject(object)
                .setActionStatus(Action.STATUS_TYPE_COMPLETED)
                .build();
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        AppIndex.AppIndexApi.start(client, getIndexApiAction());
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        AppIndex.AppIndexApi.end(client, getIndexApiAction());
        client.disconnect();
    }
}
