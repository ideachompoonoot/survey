package com.example.chompoonoot.surveys;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import com.example.chompoonoot.surveys.model.SurveyResponse;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class SurveyListViewAdapter extends BaseAdapter {

    LayoutInflater inflator;
    List<SurveyResponse> surveyResponseList;
    Context context;
    public SurveyListViewAdapter(LayoutInflater inflator){
        this.inflator = inflator;
    }
    public SurveyListViewAdapter(LayoutInflater inflator, List<SurveyResponse> surveyResponsesList){
        this.inflator = inflator;
        this.surveyResponseList = surveyResponsesList;
    }

    public SurveyListViewAdapter(LayoutInflater inflator, List<SurveyResponse> surveyResponsesList, Context context){
        this.inflator = inflator;
        this.surveyResponseList = surveyResponsesList;
        this.context = context;
    }

    @Override
    public int getCount() {
        return surveyResponseList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView != null) {
            holder = (ViewHolder) convertView.getTag();

        } else {
            convertView = this.inflator.inflate(R.layout.list_item_view, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        }
        convertView.setMinimumHeight(parent.getMeasuredHeight());
        holder.imageView.setMinimumHeight(parent.getMeasuredHeight());

        SurveyResponse survey = this.surveyResponseList.get(position);
        holder.titleTextView.setText(survey.getTitle());
        holder.subTitleTextView.setText(survey.getDescription());
        Log.d("ListViewAdapter", survey.getCoverImageUrl());

        Picasso.with(context).load(survey.getCoverImageUrl()).into(holder.imageView);


        return convertView;
    }

    static class ViewHolder {
        @BindView(R.id.imageView) ImageView imageView;
        @BindView(R.id.title_textview) TextView titleTextView;
        @BindView(R.id.subtitle_textview) TextView subTitleTextView;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
