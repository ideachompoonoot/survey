package com.example.chompoonoot.surveys;


import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import com.example.chompoonoot.surveys.model.SurveyResponse;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

import static com.example.chompoonoot.surveys.ConnectionManager.access_token;

public class ConnectionManager {
    final static String access_token = "6eebeac3dd1dc9c97a06985b6480471211a777b39aa4d0e03747ce6acc4a3369";
    final static String username = "usay";
    final static String password = "isc00l";
    final static String base_url = "https://www-staging.usay.co/app/";

    public void prepareDataSource(final ConnectionManagerListener listener) {

       Retrofit retrofit = new Retrofit.Builder().baseUrl(base_url).addConverterFactory(GsonConverterFactory.create()).build();
        ApiEndpoint endpoint = retrofit.create(ApiEndpoint.class);
        //String basic = encodeCredentialsForBasicAuthorization(username, password);
        Call<List<SurveyResponse>> call = endpoint.surveyList(access_token);
        call.enqueue(new Callback<List<SurveyResponse>>() {
            @Override
            public void onResponse(Call<List<SurveyResponse>> call, Response<List<SurveyResponse>> response) {
                listener.onSuccess(response);
            }

            @Override
            public void onFailure(Call<List<SurveyResponse>> call, Throwable t) {
        listener.onFailure(t);
            }
        });

    }

    private String encodeCredentialsForBasicAuthorization(String username, String password) {
        final String userAndPassword = username + ":" + password;
        return "Basic " + Base64.encodeToString(userAndPassword.getBytes(), Base64.NO_WRAP);
    }
}
interface ConnectionManagerListener{
     void onSuccess(Response<List<SurveyResponse>> response);
     void onFailure(Throwable error);
}
 interface ApiEndpoint{

    @GET("surveys.json")
    Call<List<SurveyResponse>> surveyList(@Query("access_token") String access_token);


}
